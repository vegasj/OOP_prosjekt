package Prefabs;

import application.SoundSystem;
import components.Components;
import components.Entity;
import javafx.scene.shape.Polygon;

public class PrefabController {
	
	private Polygon squarePoly = new Polygon();
	private SoundSystem sound = new SoundSystem();
	
	public PrefabController() {
		squarePoly.getPoints().addAll(new Double[]
		{ 0.0, 0.0, 50.0, 0.0, 50.0, 50.0, 0.0, 50.0 });
	}
	
	
	public Entity Player(double x, double y, double width, double height) {
		Entity player = new Entity("player", (
				Components.TRANSFORM.getValue() + 
				Components.PHYSICS.getValue() + 
				Components.COMBAT.getValue() + 
				Components.INPUT.getValue() + 
				Components.RENDERER.getValue()));
		player.setTransform(x, y, width, height);
		player.setPhysics(squarePoly);
		player.init();
		player.getRenderer().setSprite("player.png");
		player.addObserver(sound);
		return player;
	}
	public Entity Box(double x, double y, double width, double height) {
		Entity box = new Entity("Enemy", 
				Components.TRANSFORM.getValue() + 
				Components.PHYSICS.getValue() +
				Components.COMBAT.getValue() + 
				Components.RENDERER.getValue());
		box.setTransform(x, y, width, height);
		box.setPhysics(squarePoly);
		box.init();
		box.getRenderer().setSprite("box.png");
		box.addObserver(sound);
		return box;
	}
	public Entity Hexabox(double x, double y, double width, double height) {
		Polygon hexaPoly = new Polygon(
				0.0, 65,
				0.0, 25,
				40, 0.0,
				80, 25,
				80, 65,
				40, 90
				);
		Entity hexaBox = new Entity("Hexagon",
				Components.TRANSFORM.getValue() + 
				Components.PHYSICS.getValue() + 
				Components.POLYRENDERER.getValue()
				);
		hexaBox.setTransform(x, y, width, height); //TODO: Automatic width and height?
		hexaBox.setPhysics(hexaPoly);
		hexaBox.setPolyRenderer(hexaPoly);
		hexaBox.init();
		return hexaBox;
	}
	public Entity ShoveBox(double x, double y, double width, double height) {
		Entity shoveBox = new Entity("ShoveBox",
				Components.TRANSFORM.getValue() + 
				Components.PHYSICS.getValue() + 
				Components.RENDERER.getValue());
		shoveBox.setTransform(x, y, width, height);
		shoveBox.setPhysics(squarePoly);
		shoveBox.init();
		shoveBox.getRenderer().setSprite("box.png");
		return shoveBox;
	}
}
