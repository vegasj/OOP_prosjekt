package FileManagement;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import components.Entity;
import javafx.scene.Group;

public class SceneLoader  {
	
	
	private void validateSaveState(List<Entity> entities, String sceneName) {
		if(entities == null) {
			throw new IllegalArgumentException("List of entities cannot be empty when trying to save");
		}
		if(sceneName == null) {
			throw new IllegalArgumentException("Scenename cannot be null");
		}
	}
	
	
	public void saveScene(List<Entity> entities, String sceneName) {
		this.validateSaveState(entities, sceneName);
		try {
			FileOutputStream fileOut = new FileOutputStream(sceneName + ".ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(entities);
			out.close();
			fileOut.close();
			//System.out.println("Successfully saved: " + sceneName + " to: " + sceneName + ".ser");
		} catch(IOException i) {
			System.out.println("Error when trying to serialize: " + sceneName + ": " + i);
		}
	}
	
	public ArrayList<Entity> loadScene(String sceneName) {
		try {
			FileInputStream fileIn = new FileInputStream(sceneName + ".ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<Entity> listFromFile = (ArrayList<Entity>) in.readObject();
			in.close();
			fileIn.close();
			//System.out.println("Successfully loaded: " + listFromFile);
			return listFromFile;
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
