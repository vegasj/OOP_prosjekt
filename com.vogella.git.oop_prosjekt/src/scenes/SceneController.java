package scenes;

import java.util.ArrayList;
import java.util.HashMap;

import FileManagement.SceneLoader;
import components.Entity;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

/**
 * Recommended use of this class:
 * Instantiate a new SceneController object, and only declare sceneName
 * Create entities seperatly, either adding one by one to the scene by calling sceneController.addEntityToScene(Entitiy) or 
 * sceneController.setEntitiesToScene(List<Entity> entities).
 * If you need to do something with the entities in the current scene call sceneController.getEntities() and change one of these
 * @param sceneName
 *
 */

public class SceneController {
	
	SceneLoader sceneLoader;
	
	//We use a hashmap with keys of scene name and value of root node
	private HashMap<String, Group> sceneMap = new HashMap<>();
	
	//This hashmap stores SceneName along with a list of entities in these scenes
	private HashMap<String, ArrayList<Entity>> entityMap = new HashMap<>();
	
	private Scene scene; //Currently active scene
	private Group currentRoot; //Currently active root
	private String activeSceneName; //Currently active scene
	
	
	//Constructors
	public SceneController(Group root, String scene_name, int WINDOW_WIDTH, int WINDOW_HEIGHT) {
		sceneLoader = new SceneLoader();
		
		this.currentRoot = root;
		this.addScene(scene_name, root);
		this.scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT, Color.BLACK);
		activeSceneName = scene_name;
	}
	public SceneController(String sceneName) {
		activeSceneName = sceneName;
		sceneLoader = new SceneLoader();
	}
	
	//Checkers
	private boolean isSceneSaved(String name) {
		return this.sceneMap.containsKey(name);
	}
	
	//Validators
	private void validateName(String name) {
		if(!this.sceneMap.containsKey(name)) {
			throw new IllegalArgumentException(name + " is not in list of scenes. Availible scenes are: " + this.sceneMap.toString());
		}
	}
	
	private boolean sceneInMap(String scene) {
		return(this.sceneMap.containsKey(scene));
	}
	
	//Setters
	public void setRootToScene(String scene, Group root) {
		this.sceneMap.put(scene, root);
	}
	
	public void addScene(String name) {
		this.addScene(name, this.getRootNode());
	}
	
	public void addScene(String name, Group root) {
		this.sceneMap.put(name, root);
	}
	
	public void addNodeToScene(Node e, String sceneName) {
		if(!this.sceneInMap(sceneName)) {
			this.sceneMap.put(sceneName, new Group());
		}
		Group rootToAddTo = this.sceneMap.get(sceneName);
		rootToAddTo.getChildren().add(e);
	}
	
	public void addNodeToScene(Node e) {
		this.currentRoot.getChildren().add(e);
	}
	
	//Add entities to the currently active scene hashmap key
	public void addEntityToScene(Entity e) {
		ArrayList<Entity> currentEntitiesInScene;
		if(this.getEntitiesInCurrentScene() != null) {
			currentEntitiesInScene = this.getEntitiesInCurrentScene();
		}else {
			currentEntitiesInScene = new ArrayList<>();
		}

		currentEntitiesInScene.add(e);
		this.setEntitiesToScene(currentEntitiesInScene, activeSceneName);
	}
	
	public void addEntityToScene(Entity e, String name) {
		ArrayList<Entity> currentEntitiesInScene;
		if(this.getEntitiesInScene(name) != null) {
			currentEntitiesInScene = this.getEntitiesInScene(name);
		}else {
			currentEntitiesInScene = new ArrayList<>();
		}
		currentEntitiesInScene.add(e);
		this.setEntitiesToScene(currentEntitiesInScene, name);
	}
	
	public void setEntitiesToScene(ArrayList<Entity> e) {
		this.setEntitiesToScene(e, activeSceneName);
	}
	
	public void setEntitiesToScene(ArrayList<Entity> e, String scene) {
		if(!this.isSceneSaved(scene)) {
			this.addScene(scene);
		}
		this.entityMap.put(scene, e);
		this.saveScene(scene);
	}
	
	//Getters
	public Group getRootNode(String name) {
		return this.sceneMap.get(name);
	}
	
	public Group getRootNode()
	{
		return this.currentRoot;
	}
	
	public Scene getCurrentScene() {
		return this.scene;
	}
	
	//Retrieve a list of entities in current scene
	public ArrayList<Entity> getEntitiesInCurrentScene(){
		return this.entityMap.get(activeSceneName);
	}
	
	public ArrayList<Entity> getEntitiesInScene(String sceneName){
		return this.entityMap.get(sceneName);
	}
	
	
	//Scene management
	
	public void removeScreen(String name) {
		this.validateName(name);
		this.sceneMap.remove(name);
	}
	
	public void activate(String name) {
		this.validateName(name);
		this.currentRoot = this.sceneMap.get(name);
		//this.scene.setRoot(this.currentRoot);
		activeSceneName = name;
		System.out.println("New active scene: " + activeSceneName);
	}
	
	//Scene storage management
	
	public void saveScene(String sceneName) {
		this.validateName(sceneName);
		this.sceneLoader.saveScene(getEntitiesInScene(sceneName), sceneName);
	}
	
	public void saveScene() {
		this.sceneLoader.saveScene(getEntitiesInCurrentScene(), activeSceneName);
	}
	
	
	public void loadScene(String sceneName) {
		this.validateName(sceneName);
		ArrayList<Entity> sceneEntities = this.sceneLoader.loadScene(sceneName);
		this.setEntitiesToScene(sceneEntities, sceneName);
		this.activate(sceneName);
	}
	
	@Override
	public String toString() {
		return String.format("SceneController: \n Current Scenes are: %s", this.sceneMap.toString());
	}
}
