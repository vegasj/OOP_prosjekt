package scenes;

import java.util.ArrayList;

import Prefabs.PrefabController;
import application.Main;
import components.Entity;

public class LevelCreator {
	
	private PrefabController prefabController = new PrefabController();
	private SceneController sceneController = Main.sceneController;
	
	private double WINDOW_WIDTH, WINDOW_HEIGHT;
	
	public LevelCreator(double WINDOW_WIDTH, double WINDOW_HEIGHT) {
		this.WINDOW_WIDTH = WINDOW_WIDTH;
		this.WINDOW_HEIGHT = WINDOW_HEIGHT;
		this.Level1();
		this.Level2();
	}
	
	private void Level1() {
		ArrayList<Entity> gameObjects = new ArrayList<>();
		
		Entity player = prefabController.Player(WINDOW_WIDTH / 2 - 25, WINDOW_HEIGHT / 2 - 25, 50, 50);
		Entity box = prefabController.Box(500 + 200, 500 + 200, 50, 50);
		Entity hexaBox = prefabController.Hexabox(WINDOW_WIDTH * 0.8, WINDOW_HEIGHT * 0.4, 80, 90);
		Entity shoveBox = prefabController.ShoveBox(500 + WINDOW_WIDTH, 500 + WINDOW_HEIGHT / 2, 50, 50);
		
		gameObjects.add(player);
		gameObjects.add(box);
		gameObjects.add(hexaBox);
		gameObjects.add(shoveBox);
		
		this.sceneController.setEntitiesToScene(gameObjects, "1"); //This function creates a new scene, setsEntities and saves scenes
	}
	private void Level2() {
		ArrayList<Entity> gameObjects = new ArrayList<>();
		
		Entity player = prefabController.Player(WINDOW_WIDTH / 2 - 25, WINDOW_HEIGHT / 2 - 25, 50, 50);
		
		gameObjects.add(player);
		
		this.sceneController.setEntitiesToScene(gameObjects, "2"); //This function creates a new scene, setsEntities and saves scenes
	}
}
