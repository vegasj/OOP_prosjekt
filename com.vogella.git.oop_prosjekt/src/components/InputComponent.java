package components;

import java.util.Arrays;

import application.Input;
import application.Main;
import javafx.scene.input.KeyCode;

public class InputComponent extends Component
{
	private static final long serialVersionUID = -1578243310196425043L;
	
	Entity owner;
	PhysicsComponent physics;
	
	/* These should be in a different component */
	private double walkSpeed;
	private double runSpeed;
	
	private boolean SHIFT = false;
	
	public String toString()
	{
		return String.format("Input Component.\nOwner: %s", owner);
	}
	
	public InputComponent(Entity owner)
	{
		this.owner = owner;
	}
	
	@Override
	public void start()
	{
		physics = owner.getPhysics();
		
		walkSpeed = 5;
		runSpeed = 10;
	}
	
	public void update(double deltaTime)
	{
		if (!(Input.getKeyboardInputs() == null))
		{
			SHIFT = Arrays.asList(Input.getKeyboardInputs()).contains(KeyCode.SHIFT);
			for (KeyCode key : Input.getKeyboardInputs())
			{

				switch(key) {
				case W:
					physics.addYAccel(SHIFT ? -runSpeed : -walkSpeed);
					break;
				case S:
					physics.addYAccel(SHIFT ? runSpeed : walkSpeed);
					break;
				case A:
					physics.addXAccel(SHIFT ? -runSpeed : -walkSpeed);
					break;
				case D:
					physics.addXAccel(SHIFT ? runSpeed : walkSpeed);
					break;
				case H:
					Main.physicsDebug = !Main.physicsDebug;
					break;
				case Y: //Testing scenes
					Main.sceneController.loadScene("2");
					break;
				default:
					break;
				}
			}
		}
	}
}