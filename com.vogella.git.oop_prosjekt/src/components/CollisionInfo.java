package components;

import serializable.Point2D;

public class CollisionInfo
{
	public Entity objCollidedWith = null;
	public double distance = 0;					//Amount of overlap in collision
	public Point2D vector = new Point2D(0,0); 	//The direction of the collision - unit vector (0.0 - 1.0)
	
	public String toString()
	{
		return String.format("Collided with: %s Vector x: %.3f y: %.3f  Distance: %.3f", objCollidedWith, vector.getX(), vector.getY(), distance);
	}
}