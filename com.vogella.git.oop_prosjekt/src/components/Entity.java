package components;


import java.io.Serializable;
import java.util.ArrayList;

import application.GameEvent;
import application.Main;
import application.Observer;
import javafx.scene.shape.Polygon;

// An entity can be anything depending on the components associated with it, player, enemy, end-of-level-zone, sprites etc.

public class Entity implements Serializable
{
	private static final long serialVersionUID = 7492528592665320359L;
	
	private String tag = "";
	private TransformComponent tf;
	private PhysicsComponent physics;
	private CombatComponent combat;
	private InputComponent input;
	private RendererComponent renderer;
	private PolyRenderer polyRenderer;

	private ArrayList<Observer> observers = new ArrayList<>();

	public Entity(String tag, int flags)
	{
		this(flags);

		Main.addObject(this);
		this.tag = tag;
	}

	public Entity(int flags)
	{
		Main.addObject(this);

		if ((flags & Components.TRANSFORM.getValue()) == Components.TRANSFORM.getValue())
		{
			addTransform();
		}

		if ((flags & Components.PHYSICS.getValue()) == Components.PHYSICS.getValue())
		{
			addPhysics();
		}

		if ((flags & Components.COMBAT.getValue()) == Components.COMBAT.getValue())
		{
			addCombat();
		}

		if ((flags & Components.INPUT.getValue()) == Components.INPUT.getValue())
		{
			addInput();
		}

		if ((flags & Components.RENDERER.getValue()) == Components.RENDERER.getValue())
		{
			addRenderer();
		}
		
		if ((flags & Components.POLYRENDERER.getValue()) == Components.POLYRENDERER.getValue())
		{
			addPolyRenderer();
		}
	}


	public Entity(String tag)
	{
		Main.addObject(this);
		this.tag = tag;
	}

	public String toString()
	{
		return String.format("Entity Tag: %s", tag);
	}

	public void addObserver(Observer observer)
	{
		if (!observers.contains(observer))
		{
			observers.add(observer);
		}
		else
		{
			System.out.println("Observer already registered.");
		}
	}

	public void removeObserver(Observer observer)
	{
		observers.remove(observer);
	}

	/**
	 * Notifies all observers of this object about an event.
	 * 
	 * @param entity The entity notifying about an event.
	 * @param event  The GameEvent event being notified about.
	 */
	protected void notify(Entity entity, GameEvent event)
	{
		for (Observer observer : observers)
		{
			observer.onNotify(entity, event);
		}
	}

	/**
	 * Call this after instantiating an entity with components. Used to setup
	 * component-component references. For example, the PhysicsComponent needs a
	 * reference to the Transform etc.
	 */
	public void init()
	{
		if (tf != null)
		{
			tf.start();
		}
		if (physics != null)
		{
			physics.start();
		}
		if (input != null)
		{
			input.start();
		}
		if (renderer != null)
		{
			renderer.start();
		}
	}

	public void update(double deltaTime)
	{
		if (tf != null)
		{
			tf.update(deltaTime);
		}
		if (physics != null)
		{
			physics.update(deltaTime);
		}
		if (combat != null)
		{
			combat.update(deltaTime);
		}
		if (input != null)
		{
			input.update(deltaTime);
		}
		if (renderer != null)
		{
			renderer.update(deltaTime);
		}
		if (polyRenderer != null)
		{
			polyRenderer.update(deltaTime);
		}
	}

	public String getTag()
	{
		if (tag.equals(""))
		{
			return "Obj with no tag";
		}
		return tag;
	}

	private void addTransform()
	{
		if (tf == null)
		{
			this.tf = new TransformComponent(this, 50, 50, 50, 50);
		}
	}

	public void setTransform(double x, double y, double width, double height)
	{
		if (tf == null)
		{
			this.tf = new TransformComponent(this, x, y, width, height);
		}

		else
		{
			tf.setPosition(x, y);
			tf.setWidth(width);
			tf.setHeight(height);
		}
	}

	private void addPhysics()
	{
		if (physics == null)
		{
			this.physics = new PhysicsComponent(this);
		}
		else
		{
			System.out.println("Already has physics component");
		}
	}

	private void addCombat()
	{
		if (combat == null)
		{
			this.combat = new CombatComponent(this);
		}
		else
		{
			System.out.println("Already has Combat Component");
		}
	}

	public void setPhysics(Polygon poly)
	{
		if (physics == null)
		{
			this.physics = new PhysicsComponent(this, poly);
		}
		else
		{
			physics.setPolygon(poly);
		}
	}

	private void addInput()
	{
		if (input == null)
		{
			this.input = new InputComponent(this);
		}
	}

	private void addRenderer()
	{
		if (renderer == null)
		{
			this.renderer = new RendererComponent(this);
		}
	}
	
	private void addPolyRenderer()
	{
		Polygon defaultPoly = new Polygon();
		defaultPoly.getPoints().addAll(new Double[]
				{ 0.0, 0.0, 50.0, 0.0, 50.0, 50.0, 0.0, 50.0 });
		
		this.polyRenderer = new PolyRenderer(this, defaultPoly);
	}
	
	public void setPolyRenderer(Polygon poly)
	{
		polyRenderer.setPolygon(poly);
	}

	public TransformComponent tf()
	{
		return tf;
	}

	public PhysicsComponent getPhysics()
	{
		return physics;
	}

	public CombatComponent getCombat()
	{
		return combat;
	}

	public InputComponent getInput()
	{
		return input;
	}

	public RendererComponent getRenderer()
	{
		return renderer;
	}
}