package components;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import application.Main;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public class RendererComponent extends Component
{
	private static final long serialVersionUID = 4054261021949817991L;
	
	private Entity owner;
	private TransformComponent tf;
	
	private transient Point2D pos;
	private transient Image sprite;

	private String img_src;

	
	public RendererComponent(Entity owner)
	{
		this.owner = owner;	
	}
	
	public String toString()
	{
		return String.format("Renderer Component \nOwner: %s", owner);
	}
	
	public void start()
	{
		tf = owner.tf();
	}
	
	public void update(double deltaTime)
	{
		//Rendering
		pos = tf.getPosition();
		
		if (sprite == null)
		{
			this.setSprite(img_src);
		}
		else
		{
			Main.gc.drawImage(sprite, pos.getX(), pos.getY(), tf.width(), tf.height());
		}
	}
	
	public void setSprite(String filePath)
	{
		img_src = filePath;
		FileInputStream inpStream = null;
		try
		{
			if(img_src == null) {
				inpStream = new FileInputStream("src/resources/" + "null.png");
			}else {
				inpStream = new FileInputStream("src/resources/" + img_src);
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		sprite = new Image(inpStream, tf.width(), tf.height(), false, false);
	}
}
