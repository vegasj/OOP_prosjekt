package components;

import java.awt.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import static application.MoreMath.*;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import serializable.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class PolyCollider implements Serializable
{
	private static final long serialVersionUID = -6874596546266455339L;

	// The entity owner of this Collision Box
	private Entity owner;
	public boolean active = true;

	// The Collision Box vertices and its offsets from the owners transform
	private transient Polygon poly; // Can be constructed from the serializableVertices variable
	private transient Point2D[] vertices;
	
	private Double[] serializableVertices;	// Stores as vertex 1's x, y, vertex 2's x, y etc..
	
	private int nVertices;
	private double xOffset = 0;
	private double yOffset = 0;
	
	private double debugLineLength = 40;

	/* Gives a standard 50x50 boxCollider */
	public PolyCollider(Entity owner, double xOffset, double yOffset)
	{
		this(owner, xOffset, yOffset, new Polygon(0.0, 0.0, 50.0, 0.0, 50.0, 50.0, 0.0, 50.0));
	}

	/* Set custom convex-polygonCollider */
	public PolyCollider(Entity owner, double xOffset, double yOffset, Polygon poly)
	{
		this.owner = owner;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		setPolygon(poly);
	}

	public void setOffsets(Point2D offset)
	{
		xOffset = offset.getX();
		yOffset = offset.getY();
	}

	public Point2D getOffsets()
	{
		return new Point2D(xOffset, yOffset);
	}

	public Point2D getPosition()
	{
		double x = this.owner.tf().xPosition() + xOffset;
		double y = this.owner.tf().yPosition() + yOffset;

		return new Point2D(x, y);
	}

	/**
	 * Call this from the corresponding PhysicsComponent.
	 * 
	 * @return The CollisionInfo of a collision, if any is detected, otherwise
	 *         returns null.
	 */
	public CollisionInfo[] getCollisions()
	{
		if (!active)
		{
			return null;
		}

		ArrayList<CollisionInfo> info = new ArrayList<CollisionInfo>();
		for (Entity object : Main.getCollisionBoxes(this.owner))
		{
			CollisionInfo temp = collisionTest(object);

			if (temp != null)
			{
				info.add(temp);
			}
		}

		CollisionInfo[] returnInfo = info.toArray(new CollisionInfo[info.size()]);
		return returnInfo;
	}

	public void setPolygon(Polygon polygon)
	{
		this.poly = polygon;
		
		/* Getting the vertices */
		ObservableList<Double> rawVertices = polygon.getPoints();
		nVertices = rawVertices.size() / 2;
		vertices = new Point2D[nVertices];

		serializableVertices = new Double[2*nVertices];
    
		for (int i = 0; i < nVertices; i++)
		{
			vertices[i] = new Point2D(rawVertices.get(2 * i), rawVertices.get(2 * i + 1));

			serializableVertices[2 * i] = vertices[i].getX();
			serializableVertices[2 * i + 1] = vertices[i].getY();
		}
	}

	/**
	 * The collision poly's raw vertex data
	 * 
	 * @return Raw vertex data in the form of pairs of doubles
	 */
	public Polygon getVertices()
	{
		return poly;
	}

	/**
	 * Uses the Separating Axis Theorem (SAT) to test for collision between any
	 * convex polygons. Reference: https://www.sevenson.com.au/actionscript/sat/
	 * 
	 * @param The object to collision test with.
	 * @return CollisionInfo on the collision. Return null if no collision is
	 *         detected.
	 */
	public CollisionInfo collisionTest(Entity obj)
	{
		ObservableList<Double> rawVertices;
		/* Getting vertexes of object to test collision with */
		if(obj.getPhysics().getPolygon() != null) {
			rawVertices = obj.getPhysics().getPolygon().getPoints();
		} else {
			ArrayList<Double> serializedVerticesList = new ArrayList<Double>(Arrays.asList(serializableVertices));
			rawVertices = FXCollections.observableArrayList(serializedVerticesList);
			this.vertices = this.doublearrayToPoint2D(serializableVertices);
		}
		
		int objNumVertices = rawVertices.size() / 2; // Divide by two since each vertex is stores as a pair of doubles
		Point2D[] objVertices = new Point2D[objNumVertices];
		for (int i = 0; i < objNumVertices; i++)
		{
			objVertices[i] = new Point2D(rawVertices.get(2 * i), rawVertices.get(2 * i + 1));
		}

		// Stores the biggest overlap over all edges so we move the polygon outside the
		// collision
		double shadowOverlap = Double.MAX_VALUE;
		Point2D smallestOverlapAxis = new Point2D(0, 0); // The normal-/directional-vector corresponding with the least
															// overlap

		// Object to return - containing info about the collision
		CollisionInfo result = new CollisionInfo();

		/* Drawing vertexes for debugging */
		if (Main.physicsDebug)
		{
			Main.gc.setFill(Color.RED);
			for (Point2D vertex : vertices)
			{
				Main.gc.fillOval(xOffset + owner.tf().xPosition() + vertex.getX() - 2.5,
						yOffset + owner.tf().yPosition() + vertex.getY() - 2.5, 5, 5);
			}
		}

		/*
		 * Loops through every edge of one object, and saves the highest and lowest
		 * value polygon relative to that edge for both objects, if there's a gap, we
		 * can break the loop, there is no collision. If not, store all the values and
		 * calculate how far into each other they are later.
		 */

		/* Checking each edge of this objects polygon */
		for (int i = 0; i < nVertices; i++)
		{
			/* STEP 1: Taking an edge of the polygon and getting a normal of that edge */

			// Indexes for vertices of edge
			int p1 = i;
			int p2 = i + 1;
			// If the index of the second polygon of the current edge is the same as
			// nVertices, we've gone full circle, so set to
			p2 = (p2 == nVertices ? 0 : p2);

			Point2D axis = vectorBetweenPoints(vertices[p2], vertices[p1]);
			
			/* Getting the unit normal of the edge */
			axis = unitNormal(axis);

			// Debugging: Draws normal vectors
			if (Main.physicsDebug)
			{
				Main.gc.beginPath();
				Main.gc.setLineWidth(5);
				Main.gc.setStroke(Color.RED);
				double midX = (vertices[p2].getX() + vertices[p1].getX()) / 2 + xOffset + owner.tf().xPosition();
				double midY = (vertices[p2].getY() + vertices[p1].getY()) / 2 + yOffset + owner.tf().yPosition();
				Main.gc.moveTo(midX, midY);
				Main.gc.lineTo(midX + axis.getX() * debugLineLength, midY + axis.getY() * debugLineLength);
				Main.gc.stroke();
				Main.gc.closePath();
			}

			/*
			 * STEP 2: Loop through every vertex and project in onto the current "axis"-edge
			 * by using dot-product. Keep track of highest and lowest value vertex relative
			 * to the current "axis".
			 */

			// Initial min-max values for first shape.
			double p1Min = dotProduct(axis, vertices[0]);
			double p1Max = p1Min;
			// Finding min-max values for first shape
			for (int j = 1; j < nVertices; j++)
			{
				double dot = dotProduct(axis, vertices[j]);
				p1Min = Math.min(p1Min, dot);
				p1Max = Math.max(p1Max, dot);
			}

			// Initial min-max values for second shape.
			double p2Min = dotProduct(axis, objVertices[0]);
			double p2Max = p2Min;
			// Finding min-max values for first shape
			for (int j = 1; j < objNumVertices; j++)
			{
				double dot = dotProduct(axis, objVertices[j]);
				p2Min = Math.min(p2Min, dot);
				p2Max = Math.max(p2Max, dot);
			}

			/*
			 * Now we only need to add the offset between the vertices, the distance between
			 * the shapes, before looking for any gaps in the projection
			 */
			// The offset
			Point2D vOffset = new Point2D(xOffset + owner.tf().xPosition() - obj.tf().xPosition(),
					-(yOffset + owner.tf().yPosition() - obj.tf().yPosition()));
			// Projecting the offset onto the current axis
			double sOffset = dotProduct(axis, vOffset);

			// Adding the correct offset
			p2Min += sOffset;
			p2Max += sOffset;

			/* Overlap test of the min and max of both polygons */
			if ((p1Min - p2Max > 0) || (p2Min - p1Max > 0))
			{
				// There is a gap => there is no collision.
				return null;
			}
			else
			{
				if (Math.abs(p2Min - p1Max) < shadowOverlap)
				{
					shadowOverlap = Math.abs(p2Min - p1Max);
					smallestOverlapAxis = axis;
				}
			}
		}

		/*--------------------------*/

		/* Checking each edge of the second polygon */
		for (int i = 0; i < objNumVertices; i++)
		{
			/* STEP 1: Taking an edge of the polygon and getting a normal of that edge */
			int p1 = i;
			int p2 = i + 1;
			p2 = (p2 == objNumVertices ? 0 : p2);

			Point2D axis = vectorBetweenPoints(objVertices[p1], objVertices[p2]);

			/* Getting the unit normal of the edge */
			axis = unitNormal(axis);

			/*
			 * STEP 2: Loop through every vertex and project in onto the current "axis" by
			 * using dot-product. Keep track of highest and lowest value vertex relative to
			 * the current "axis".
			 */

			// Initial min-max values for first shape.
			double p1Min = dotProduct(axis, objVertices[0]);
			double p1Max = p1Min;
			// Finding min-max values for first shape
			for (int j = 1; j < objNumVertices; j++)
			{
				double dot = dotProduct(axis, objVertices[j]);
				p1Min = Math.min(p1Min, dot);
				p1Max = Math.max(p1Max, dot);
			}

			// Initial min-max values for second shape.
			double p2Min = dotProduct(axis, vertices[0]);
			double p2Max = p2Min;
			// Finding min-max values for second shape
			for (int j = 1; j < nVertices; j++)
			{
				double dot = dotProduct(axis, vertices[j]);
				p2Min = Math.min(p2Min, dot);
				p2Max = Math.max(p2Max, dot);
			}

			/*
			 * Now we only need to add the offset between the vertices, the distance between
			 * the shapes, before looking for any gaps in the projection
			 */
			// The offset
			Point2D vOffset = new Point2D(obj.tf().getPosition().getX() - xOffset - owner.tf().getPosition().getX(),
					-(obj.tf().getPosition().getY() - yOffset - owner.tf().getPosition().getY()));
			// Projecting the offset onto the current axis
			double sOffset = dotProduct(axis, vOffset);

			// Adding the correct offset
			p2Min += sOffset;
			p2Max += sOffset;

			/* Overlap test of the min and max of both polygons */
			if ((p1Min - p2Max > 0) || (p2Min - p1Max > 0))
			{
				// There is a gap => there is no collision.
				return null;
			}
			else
			{
				// If current overlap is bigger than previously saved overlap, replace it
				if (Math.abs(p1Min - p2Max) < shadowOverlap)
				{
					shadowOverlap = Math.abs(p1Min - p2Max);
					smallestOverlapAxis = axis;
				}
			}
		}

		if (shadowOverlap != Double.MAX_VALUE)
		{
			result.objCollidedWith = obj;
		}
		result.distance = shadowOverlap;
		result.vector = smallestOverlapAxis;
		return result;

	}
	
	private Point2D[] doublearrayToPoint2D(Double[] array) {
		Point2D[] returnPoint = new Point2D[array.length/2];
		for(int i=0; i<array.length;i+=2) {
			returnPoint[i/2] = new Point2D(array[i], array[i+1]);
		}
		return returnPoint;
	}
}