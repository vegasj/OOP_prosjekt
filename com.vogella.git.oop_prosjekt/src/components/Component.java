package components;

import java.io.Serializable;

public abstract class Component implements Serializable
{
	private static final long serialVersionUID = 6259457746339016755L;
	public abstract void update(double deltaTime);
	public abstract void start();
}