package components;

import application.GameEvent;
import javafx.scene.shape.Polygon;

public class PhysicsComponent extends Component
{
	private static final long serialVersionUID = -2682219816218319739L;
	
	// private double LINE_LENGTH = 15;
	private Entity owner;
	private TransformComponent tf;	
	private PolyCollider collisionPoly;

	private double accelSpeed = 200;
	private double xAccel;
	private double yAccel;

	private double xVelocity;
	private double yVelocity;

	private double friction = 0.9; // Lower = more friction. Max velocity = acceleration/friction
	private double fallOff = 0.95; // Lower = faster fall off time
	private double speedDeadZone = 1; // If velocity is under this threshold, set it to 0
	private double temp = 0.02;

	public PhysicsComponent(Entity owner)
	{
		this.owner = owner;
		collisionPoly = new PolyCollider(owner, 0, 0);
	}

	public PhysicsComponent(Entity owner, Polygon poly)
	{
		this.owner = owner;
		collisionPoly = new PolyCollider(owner, 0, 0, poly);
		setPolygon(poly);
	}

	public String toString()
	{
		return String.format("PhysicsComponent\nOwner: %s xVelocity: %.3f yVelocity: %.3f", owner, xVelocity,
				yVelocity);
	}

	@Override
	public void start()
	{
		this.tf = owner.tf();

		xVelocity = 0;
		xVelocity = 0;
	}

	public void update(double deltaTime)
	{
		/* Normalizing diagonal speed */
		if (Math.abs(xVelocity) > 0 && Math.abs(yVelocity) > 0)
		{
			xVelocity += xAccel * deltaTime * 0.7071067811865;
			yVelocity += yAccel * deltaTime * 0.7071067811865;
		}
		else
		{
			xVelocity += xAccel * deltaTime;
			yVelocity += yAccel * deltaTime;
		}

		/* Reset acceleration after its applied */
		xAccel = 0;
		yAccel = 0;

		/* Move the entity */
		tf.translate(xVelocity * deltaTime, yVelocity * deltaTime);

		/* Collision test and move this object to resolve collision */
		CollisionInfo[] info = collisionPoly.getCollisions();
		for (CollisionInfo collision : info)
		{
			if (collision != null)
			{
				//Check if colliding with 
				if(this.owner.getTag().equals("player") && collision.objCollidedWith.getTag().equals("Stairs")) {
					//Load level
				}
				tf.translate(collision.vector.getX() * collision.distance,
						-collision.vector.getY() * collision.distance);

				
				owner.notify(owner, GameEvent.PLAYER_COLLIDED);
			}
		}

		/* Applies friction */
		xVelocity *= Math.pow(friction, deltaTime + temp) * fallOff;
		yVelocity *= Math.pow(friction, deltaTime + temp) * fallOff;

		/*
		 * A dead-zone for the velocity so it doesn't take forever to stop completely.
		 * (Pixel drift effect)
		 */
		if (Math.abs(xVelocity) < speedDeadZone)
		{
			xVelocity = 0;
		}
		if (Math.abs(yVelocity) < speedDeadZone)
		{
			yVelocity = 0;
		}
	}

	public Polygon getPolygon()
	{
		return collisionPoly.getVertices();
	}

	public void setPolygon(Polygon polygon)
	{
		collisionPoly.setPolygon(polygon);
	}

	public double xVel()
	{
		return xVelocity;
	}

	public double yVel()
	{
		return yVelocity;
	}

	public void addXAccel(double xAccel)
	{
		this.xAccel = xAccel * accelSpeed;
	}

	public void addYAccel(double yAccel)
	{
		this.yAccel = yAccel * accelSpeed;
	}
}