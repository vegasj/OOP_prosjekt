package components;

import application.Main;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class PolyRenderer extends Component
{
	private static final long serialVersionUID = 7457403065865806522L;

	private Entity owner;
	private transient Color color;
	
	// Storing RGBA values of color for serialization
	private double red;
	private double green;
	private double blue;
	private double alpha;
	
	private double[] xPoints;
	private double[] yPoints;
	private Object[] rawVertices;
	private int  nVertices;
	
	public PolyRenderer(Entity owner, Polygon poly)
	{
		this.owner = owner;

		this.rawVertices = poly.getPoints().toArray();
		this.nVertices = poly.getPoints().size() / 2;
		
		xPoints = new double[nVertices];
		yPoints = new double[nVertices];
		
		this.color = Color.YELLOW;
		
		red = color.getRed();
		green = color.getGreen();
		blue = color.getBlue();
		alpha = color.getOpacity();
	}
	
	public void setPolygon(Polygon poly)
	{
		this.rawVertices = poly.getPoints().toArray();
		this.nVertices = poly.getPoints().size() / 2;
		
		xPoints = new double[nVertices];
		yPoints = new double[nVertices];
	}
	
	public void setPolygon(Polygon poly, Color color)
	{
		setPolygon(poly);
		if(this.color != null) {
			this.color = color;
		}else {
			this.color = new Color(red, green, blue, alpha);
		}
	}
	
	public void start()
	{
	}
	
	public void update(double deltaTime)
	{
		// Updates position off all vertices to draw polygon with
		for (int i = 0; i < nVertices * 2; i += 2)
		{
			xPoints[i / 2] = (double) rawVertices[i] + owner.tf().xPosition();
			yPoints[i / 2] = (double) rawVertices[i + 1] + owner.tf().yPosition();
		}
		
		Main.gc.setFill(color);
		Main.gc.fillPolygon(xPoints, yPoints, nVertices);
	}
}
