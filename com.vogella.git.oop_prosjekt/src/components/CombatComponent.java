package components;

import java.util.ArrayList;

import application.GameEvent;
import application.Input;
import application.Main;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import serializable.Point2D;


public class CombatComponent extends Component
{
	private static final long serialVersionUID = 2218571489795586068L;

	private Entity owner;
	private PolyCollider attackCollisionBox;
	private Point2D attackOffset;

	private double attackDuration = 0.3; // How long basic attack lasts
	private double attackConeAngle = 120; // How many angles the cone of attack should be
	// Calculates the increments based on the above values
	private double attackAngleIncrements = attackConeAngle / attackDuration;
	private double offsetLength = 60; // Distance the attack is away from the player

	private double attackTimer = attackDuration + 1; // How long current attack has lasted
	private ArrayList<Entity> alreadyAttacked = new ArrayList<>();

	private int health;
	private int damage;
	
	private double knockbackStrength = 50;

	public CombatComponent(Entity owner)
	{
		this.owner = owner;
		this.health = 100;
		this.damage = 50;
		this.attackOffset = new Point2D(-70, 0);
		this.attackCollisionBox = new PolyCollider(owner, attackOffset.getX(), attackOffset.getY());
	}

	public void update(double deltaTime)
	{
		/* Basic attack */
		for (MouseEvent event : Input.getMouseInputs())
		{
			if (!this.owner.getTag().equals("player"))
			{
				break;
			}
			// TODO: Is called on mouse down and up, e.g. twice per click.
			if (event.getButton() == MouseButton.PRIMARY)
			{
				double screenX = event.getSceneX();
				double screenY = event.getSceneY();

				primaryAttack(screenX, screenY);
			}
		}

		Main.gc.setFill(Color.BLUE);
		Main.gc.fillOval(attackCollisionBox.getPosition().getX() - 5, attackCollisionBox.getPosition().getY() - 5, 10,
				10);

		if (attackCollisionBox.active)
		{
			attackTimer += deltaTime;

			if (attackTimer > attackDuration)
			{
				attackCollisionBox.active = false;
				attackTimer = 0;
				attackCollisionBox.setOffsets(attackOffset);
				alreadyAttacked.clear();
				return;
			}

			// Sweeps the HitBox about the player
			Point2D newOffset = rotateAboutPoint(attackCollisionBox.getOffsets(), new Point2D(0, 0),
					attackAngleIncrements * Main.deltaTime());
			attackCollisionBox.setOffsets(newOffset);
			
			CollisionInfo[] hitInfo = attackCollisionBox.getCollisions();

			for (CollisionInfo collision : hitInfo)
			{
				if (collision != null)
				{
					CombatComponent enemyCombat = collision.objCollidedWith.getCombat();
					if (enemyCombat != null)
					{
						dealDamage(enemyCombat);
						
						/* Adds Knock-back effect */
						enemyCombat.owner.getPhysics().addXAccel(-collision.vector.getX() * knockbackStrength);
						enemyCombat.owner.getPhysics().addYAccel(-collision.vector.getY() * knockbackStrength);
					}
				}
			}
		}
	}

	private void primaryAttack(double xTarget, double yTarget)
	{
		if (attackTimer == 0)
		{
			attackCollisionBox.active = true;
		}
		else
		{
			return;
		}
		
		/* Making vector between mouse point and player position */
		double xDir = xTarget - this.owner.tf().getMiddle().getX();
		double yDir = yTarget - this.owner.tf().getMiddle().getY();

		/* Making normal vector */
		Point2D normal = new Point2D(yDir, -xDir);

		/* Normalizing normal vector */
		double length = Math.sqrt(Math.pow(normal.getX(), 2) + Math.pow(normal.getY(), 2));
		normal = new Point2D(normal.getX() / length, normal.getY() / length);
		
		// Moves the offset to correct position
		Point2D initialOffsetPos = new Point2D(normal.getX() * offsetLength, normal.getY() * offsetLength);
		attackCollisionBox.setOffsets(initialOffsetPos);
		
		// Rotates the offset so the mouse is in the middle of the sweep cone
		Point2D initialOffsetRot = rotateAboutPoint(attackCollisionBox.getOffsets(), new Point2D(0, 0), (180 - attackConeAngle)/2);
		attackCollisionBox.setOffsets(initialOffsetRot);
	}
	
	
//	private void rotateOffset(Point2D position, Point2D target)
//	{
//		Point2D currentNormal = unitNormal(position);
//		Point2D targetNormal = unitNormal(target);
//		
//		double angle = Math.acos(dotProduct(currentNormal, targetNormal));
//		
//		Rotate rot = new Rotate(0,0, angle);
//		Translate tf = new Translate();
//		Scale sc = new Scale();
//		
////		for (Transform transform : Arrays.asList(rot, tf, sc))
////		{
////			transform.transform2DPoints(attackCollisionBox.getVertices().getPoints(), 0, rotateAboutPoint(), health, damage);
////		}
//	}
	

	/**
	 * rotationAxis doesn't work currently, use rotationAxis(0,0)
	 * 
	 * @param Current       position.
	 * @param rotationAxis, the point to rotate about.
	 * @param angle,        angle in degrees.
	 * @return New x, y position based on axis of rotation and angle.
	 */
	public Point2D rotateAboutPoint(Point2D position, Point2D rotationAxis, double angle)
	{
		// Make vectors that start at origin
		double xTemp = position.getX() - rotationAxis.getX();
		double yTemp = position.getY() - rotationAxis.getY();

		// Get new positional vector from origin
		double x = xTemp * Math.cos(Math.toRadians(angle)) - yTemp * Math.sin(Math.toRadians(angle));
		double y = yTemp * Math.cos(Math.toRadians(angle)) + xTemp * Math.sin(Math.toRadians(angle));

		// Translate back to our current position
		x += rotationAxis.getX();
		y += rotationAxis.getY();

		return new Point2D(x, y);
	}

	public void attack()
	{
		if (attackTimer == 0)
		{
			attackCollisionBox.active = true;
		}
	}

	public void dealDamage(CombatComponent combat)
	{
		// Makes sure a single attack only hits an enemy once
		if (!alreadyAttacked.contains(combat.owner))
		{
			combat.receiveDamage(this.damage);
			alreadyAttacked.add(combat.owner);
		}
	}

	public void receiveDamage(int damage)
	{
		this.health -= damage;

		this.owner.notify(this.owner, GameEvent.TOOK_DAMAGE);

		if (health <= 0)
		{
			System.out.println(this.owner.getTag() + " died");
			die();
		}
	}

	/**
	 * Calls for main to remove all references to this objects owner. Essentially
	 * moving it to the void so it can be deleted by the garbage collector.
	 */
	private void die()
	{
		Main.removeObject(this.owner);
	}

	@Override
	public void start()
	{
	}
}