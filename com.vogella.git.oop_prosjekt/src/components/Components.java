package components;

public enum Components
{
	TRANSFORM(1),
	 PHYSICS(2),
	 COMBAT(4),
	 INPUT(8),
	 RENDERER(16),
	 POLYRENDERER(32);
	
	private final int flag;
	Components(int flag) { this.flag = flag; }
	public int getValue() { return flag; }
}