package components;

import application.Main;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

public class TransformComponent extends Component
{
	private static final long serialVersionUID = 3076932614399025439L;

	private Entity owner;
	
	//posX and posY are the top left of an object
	private double xPos;
	private double yPos;
	
	private double width;
	private double height;
	
	public TransformComponent(Entity owner, double posX, double posY, double width, double height)
	{
		this.owner = owner;
		this.xPos = posX;
		this.yPos = posY;
		this.width = width;
		this.height = height;
	}
	
	public String toString()
	{
		return String.format("Transform Component\nOwner: %s posX: %.3f posY: %.3f Width: %.3f Height: %.3f", owner, xPos, yPos, width, height);
	}
	
	public void translate(double x, double y)
	{
		xPos += x;
		yPos += y;
		
		if (owner.getTag().equals("player"))
		{
			Main.moveCamera(x, y);
		}
	}
	
	public void translate(Point2D pos)
	{
		xPos += pos.getX();
		yPos += pos.getX();
		
		if (owner.getTag() == "player")
		{
			Main.moveCamera(pos.getX(), pos.getY());
		}
	}
	
	public double width()
	{
		return width;
	}

	public double height()
	{
		return height;
	}
	
	public void setWidth(double width)
	{
		if (width < 0)
		{
			throw new IllegalArgumentException("Width can't be set to less than 0");
		}
		
		this.width = width;
	}

	public void setHeight(double height)
	{
		if (height < 0)
		{
			throw new IllegalArgumentException("Height can't be set to less than 0");
		}
		
		this.height = height;
	}
	
	public Point2D getPosition()
	{
		return new Point2D(xPos, yPos);
	}
	
	public void setPosition(double x, double y)
	{
		xPos = x;
		yPos = y;
	}
	
	/**Calculates xPos + width/2, yPos + height/2
	 * @return Coordinate in the middle of this object.
	 */
	public Point2D getMiddle()
	{
		return new Point2D(xPos + width/2, yPos + height/2);
	}
	
	
	/** Calculated as width / 2, height / 2
	 * @return The offset to the middle of the object from the top-left position
	 */
	public Point2D getMiddleOffset()
	{
		return new Point2D(width / 2, height / 2);
	}
	
	public void setPosition(Point2D point)
	{
		xPos = point.getX();
		yPos = point.getY();
	}
	
	public double xPosition()
	{
		return xPos;
	}
	
	public double yPosition()
	{
		return yPos;
	}

	@Override
	public void start()
	{
	}

	@Override
	public void update(double deltaTime)
	{
		Main.gc.setFill(Color.BLUE);
		Main.gc.fillOval(xPos -5, yPos -5, 10, 10);
	}
}
