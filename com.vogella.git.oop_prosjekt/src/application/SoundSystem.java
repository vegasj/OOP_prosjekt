package application;

import java.io.File;
import java.io.Serializable;

import components.Entity;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class SoundSystem implements Observer, Serializable
{
	private static final long serialVersionUID = 5038859247721828516L;
	
	// Collision bump
	private transient Media sound;
	private String sound_path;
	private transient MediaPlayer mediaPlayer;
	
	// Take damage grunt
	private transient Media sound2;
	private String sound2_path;
	private transient MediaPlayer mediaPlayer2;
	
	private double volume = 0.15;

	public SoundSystem()
	{
		sound_path = "src/resources/mechanical_bling.wav";

		
		sound2_path = "src/resources/took_damage.wav";

	}

	public void instantiateMediaPlayers() {
		sound = new Media(new File(sound_path).toURI().toString());
		mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.setVolume(volume);
		
		sound2 = new Media(new File("src/resources/took_damage.wav").toURI().toString());
		mediaPlayer2 = new MediaPlayer(sound2);
		mediaPlayer2.setVolume(volume);
	}
	
	@Override
	public void onNotify(Entity entity, GameEvent event)
	{
		if(mediaPlayer == null || mediaPlayer2 == null) {
			instantiateMediaPlayers();
		}else {
			switch (event)
			{
			case PLAYER_COLLIDED:
				if (mediaPlayer.getStatus() != MediaPlayer.Status.PLAYING)
				{
					mediaPlayer.play();
				}
				else
				{
					mediaPlayer.seek(Duration.ZERO);
				}
				break;
				
			case TOOK_DAMAGE:
				if (mediaPlayer2.getStatus() != MediaPlayer.Status.PLAYING)
				{
					mediaPlayer2.play();
				}
				else
				{
					mediaPlayer2.seek(Duration.ZERO);
				}
				break;
			}
		}
	}
}