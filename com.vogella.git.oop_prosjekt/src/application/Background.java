package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import components.Entity;
import javafx.scene.image.Image;

public class Background extends Entity
{
	private static final long serialVersionUID = 2547664709536600820L;
	
	private transient Image[][] tiles;
	private final int TILE_SIZE = 50;
	
	public Background(String tag, int xTiles, int yTiles)
	{
		super(tag);
		
		this.tiles = new Image[yTiles][xTiles];
	}
	
	public void update()
	{
		for (int i = 0; i < tiles.length; i++)
		{
			for (int j = 0; j < tiles[0].length; j++)
			{
				Main.gc.drawImage(tiles[i][j], TILE_SIZE * j, TILE_SIZE * i, TILE_SIZE, TILE_SIZE);
			}
		}
	}
	
	public void setTile(int x, int y, String filepath, boolean allTiles)
	{
		FileInputStream inpStream = null;
		try
		{
			inpStream = new FileInputStream("src/resources/" + filepath);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		Image sprite = new Image(inpStream, TILE_SIZE, TILE_SIZE, false, false);
		
		if (allTiles)
		{
			for (int i = 0; i < tiles.length; i++)
			{
				for (int j = 0; j < tiles[0].length; j++)
				{
					tiles[i][j] = sprite;
				}
			}
		}
		else
		{
			tiles[y][x] = sprite;			
		}
	}
}