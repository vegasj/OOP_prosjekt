package application;

import serializable.*;

/**A collection of useful math functions.
 * @author Marcus
 *
 */
public class MoreMath
{
	
	/**
	 * @param vec1
	 * @param vec2
	 * @return Dot Product
	 */
	public static double dotProduct(Point2D vec1, Point2D vec2)
	{
		return vec1.getX() * vec2.getX() + vec1.getY() * vec2.getY();
	}
	
	/**
	 * @param vector
	 * @return Vector length
	 */
	public static double vectorLength(Point2D vector)
	{
		return Math.sqrt(Math.pow(vector.getX(), 2) + Math.pow(vector.getY(), 2));
	}
	
	
	/** Makes a vector between two points, from point1, to point2
	 * Calculates p2.getX() - p1.getX(), p2.getY() - p1.getY()
	 * @param p1
	 * @param p2
	 * @return The vector between the two points
	 */
	public static Point2D vectorBetweenPoints(Point2D p1, Point2D p2)
	{
		return new Point2D(p2.getX() - p1.getX(), p2.getY() - p1.getY());
	}
	
	public static Point2D vectorBetweenPoints(double x1, double y1, double x2, double y2)
	{
		return new Point2D(x2 - x1, y2 - y1);
	}
	
	/** TO BE IMPLEMENTED
	 * @param position
	 * @param target
	 * @return
	 */
//	public static Point2D lookAt(Point2D position, Point2D target)
//	{
//		Point2D currentNormal = new Point2D(position.getX()/vectorLength(position), position.getY()/vectorLength(position));
//		Point2D targetNormal = new Point2D(target.getX()/vectorLength(target), target.getY()/vectorLength(target));
//		
//		double angle = Math.acos(dotProduct(currentNormal, targetNormal));
//		
//		return null;
//	}
	
	/** Returns a unit normal in the left direction relative to the given vector.
	 * @param vector
	 * @return The unit normal
	 */
	public static Point2D unitNormal(Point2D vector)
	{
		double length = vectorLength(vector);
		return new Point2D(vector.getY() / length, -vector.getX() / length);
	}
	
}