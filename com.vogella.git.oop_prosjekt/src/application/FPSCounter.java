package application;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class FPSCounter
{
	private double fpsCounterTimer = 0;
	private int frames = 0;
	private Text fpsText;

	public FPSCounter()
	{
		fpsText = new Text(Main.WINDOW_WIDTH - 150, 50, "FPS COUNTER");
		fpsText.setFont(new Font(20));
		fpsText.setFill(Color.RED);

		Main.uiElements.getChildren().add(fpsText);
	}

	public void update(double deltaTime)
	{
		if (fpsCounterTimer > 0.5)
		{
			fpsText.setText(String.format("FPS: %.0f", (frames / fpsCounterTimer)));
			frames = 0;
			fpsCounterTimer = 0;
		}
		else
		{
			frames++;
			fpsCounterTimer += deltaTime;
		}
	}

}