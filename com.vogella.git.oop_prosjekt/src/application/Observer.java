package application;

import components.Entity;

public interface Observer
{
	public void onNotify(Entity entity, GameEvent event);
}