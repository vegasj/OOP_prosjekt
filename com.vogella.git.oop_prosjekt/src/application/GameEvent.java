package application;

public enum GameEvent
{
	PLAYER_COLLIDED,
	TOOK_DAMAGE;
}