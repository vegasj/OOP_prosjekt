package application;

import java.util.ArrayList;
import java.util.Arrays;

import Prefabs.PrefabController;
import components.Components;
import components.Entity;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.ParallelCamera;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import scenes.LevelCreator;
import scenes.SceneController;

public class Main extends Application
{
	enum GameState
	{
		MAINMENU, PLAYING
	}

	// CONSTANTS
	// 16:9 aspect ratio, HD
	public final static int WINDOW_WIDTH = 1280;
	public final static int WINDOW_HEIGHT = 720;

	//Scene management
	public static SceneController sceneController;
	
	public static Stage stage;
	public static Camera camera;
	public static GraphicsContext gc;
	public static Canvas canvas;
	
	//Level management
	private LevelCreator levelCreator;
	private Background background;
	
	private Input input;

	private static double deltaTime;

	// The root of the scene graph
	public static Group root = new Group();
	// Must add all UI-elements to this group so they stay in the camera-view
	public static Pane uiElements;

	// Used to calculate deltaTime in AnimationTimer
	private long lastNanoTime = 0;
	private GameState gameState = GameState.PLAYING;
	private static ArrayList<Entity> gameObjects = new ArrayList<>();

	// Holds all objects to destroy this frame, but waits until end of frame so
	// events can resolve and to not get null-references. Cleared every frame.
	private static ArrayList<Entity> objectsToDestroy = new ArrayList<>();

	double xMoveCamera = 0;
	double yMoveCamera = 0;


	FPSCounter fpsCounter;
	SoundSystem sound = new SoundSystem();

	public static boolean physicsDebug = true;

	Entity player;
	Entity shoveBox;
	Entity pentagon;

	public final int TILE_SIZE = 50;

	public Main()
	{
		//Initialize scenecontroller with a scene main
		sceneController = new SceneController(root, "main", WINDOW_WIDTH, WINDOW_HEIGHT);
		levelCreator = new LevelCreator(WINDOW_WIDTH, WINDOW_HEIGHT);
		
		
		background = new Background("Background", 40, 40);
		background.setTile(0, 0, "stonefloor.jpg", true);

		sceneController.loadScene("1");
	}

	// Clears the screen before drawing the next frame
	public void clear(GraphicsContext gc)
	{
		gc.setFill(new Color(0, 0, 0, 1));
		gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
	}
	
	@Override
	public void start(final Stage stage)
	{
		
		// Initialize graphic stuff
		Main.stage = stage;
		root = new Group();
		uiElements = new Pane();
		
		//Set root of current scene
		sceneController.setRootToScene("main", root);
		Scene currentScene = sceneController.getCurrentScene();

		//Find entities
		player = this.getEntityByTag("player");
		shoveBox = this.getEntityByTag("ShoveBox");
		
		camera = new ParallelCamera();
		camera.setTranslateX(player.tf().xPosition() + player.tf().width() / 2 - WINDOW_WIDTH / 2);
		camera.setTranslateY(player.tf().yPosition() + player.tf().height() / 2 - WINDOW_HEIGHT / 2);

		currentScene.setCamera(camera);
		stage.setScene(currentScene);
		stage.setTitle("My Application");
		stage.show();

		canvas = new Canvas(stage.getWidth() * 4, stage.getHeight() * 4);
		gc = canvas.getGraphicsContext2D();
		
		input = new Input();

		sceneController.addNodeToScene(canvas);
		sceneController.addNodeToScene(uiElements);
		
		fpsCounter = new FPSCounter();

		/* INPUTS */
		currentScene.setOnKeyPressed(e -> { input.onKeyPressed(e); });
		currentScene.setOnKeyReleased(e -> { input.onKeyReleased(e); });

		currentScene.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> { input.onMouseClicked(e); });
		
		
		// The main program loop
		new AnimationTimer()
		{
			@Override
			public void handle(long currentNanoTime)
			{
				switch (gameState)
				{
				case MAINMENU:
					gameState = GameState.PLAYING;
					break;

				case PLAYING:
					deltaTime = (currentNanoTime - lastNanoTime) / 1e9;
					gameLoop(deltaTime);
					break;

				default:
					break;
				}

				lastNanoTime = currentNanoTime;
			}
		}.start();
	}

	private Entity getEntityByTag(String tag) {
		for(Entity gameObject : this.sceneController.getEntitiesInCurrentScene()) {
			if(gameObject.getTag().equals(tag)){
				return gameObject;
			}
		}
		System.out.println("No "+ tag +" in scene!");
		return null;
	}
	
	public static double deltaTime()
	{
		return deltaTime;
	}

	// Called from GameObject constructor
	public static void addObject(Entity object)
	{
		if (!gameObjects.contains(object))
		{
			gameObjects.add(object);	
			sceneController.setEntitiesToScene(gameObjects);
		}
	}

	public static void removeObject(Entity object)
	{
		if (gameObjects.contains(object) && !objectsToDestroy.contains(object))
		{
			objectsToDestroy.add(object);

		}
	}

	public void gameLoop(double deltaTime)
	{
		gameObjects = this.sceneController.getEntitiesInCurrentScene();
		// Clearing last frame
		clear(gc);

		background.update();
		
		/* Update fpsCounter*/
		fpsCounter.update(deltaTime);

		/* Updating all game objects in the scene */
		for (Entity object : gameObjects)
		{
			object.update(deltaTime);
		}

		/* Silly way to make the one box move across the screen */
		shoveBox.getPhysics().addXAccel(-20);

		if (shoveBox.tf().getPosition().getX() < -shoveBox.tf().width())
		{
			shoveBox.tf().setPosition(new Point2D(500 + WINDOW_WIDTH, 500 + WINDOW_HEIGHT / 2));
		}

		/* Deletes all objects that were destroyed this frame */
		gameObjects.removeAll(objectsToDestroy);
		objectsToDestroy.clear();

		/* Clear all mouse events before the next frame */
		input.clearMouseEvents();

	}

	public static void moveCamera(double x, double y)
	{
		// Moves the UI-group by the same amount so it stays in the camera view
		camera.relocate(camera.getLayoutX() + x, camera.getLayoutY() + y);
		uiElements.relocate(uiElements.getLayoutX() + x, uiElements.getLayoutY() + y);
	}

	/**
	 * This has major optimization potential
	 * 
	 * @param The object requesting collision boxes to test.
	 * @return Array of objects the caller should collision test.
	 */
	public static Entity[] getCollisionBoxes(Entity object)
	{
		Entity[] collisionObjects = new Entity[gameObjects.size()];
		int count = 0;

		for (int i = 0; i < gameObjects.size(); i++)
		{
			if (gameObjects.get(i).getPhysics() != null && !(gameObjects.get(i) == object))
			{
				collisionObjects[count] = gameObjects.get(i);
				count++;
			}
		}

		return Arrays.copyOfRange(collisionObjects, 0, count);
	}

	public static void main(final String[] args)
	{
		Main.launch(args);
	}
}