package application;

import java.util.ArrayList;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class Input
{
	private static ArrayList<KeyCode> keyboardInputs;
	private static ArrayList<MouseEvent> mouseInputs;

	public Input()
	{
		keyboardInputs = new ArrayList<>();
		mouseInputs = new ArrayList<>();
	}
	
	/**
	 * @return An array of all KeyBoard inputs as KeyCode.
	 * Returns null if there were no keyboard inputs this frame.
	 */
	public static KeyCode[] getKeyboardInputs()
	{
		if (keyboardInputs.size() == 0)
		{
			return null;
		}
		
		KeyCode[] returnArray = keyboardInputs.toArray(new KeyCode[mouseInputs.size()]);
		return returnArray;
	}
	
	public static MouseEvent[] getMouseInputs()
	{
		if (mouseInputs.size() == 0)
		{
			
		}
		
		MouseEvent[] returnArray = mouseInputs.toArray(new MouseEvent[mouseInputs.size()]);
		return returnArray;
	}

	public void onKeyPressed(KeyEvent e)
	{
		KeyCode code = e.getCode();

		// only add once... prevent duplicates
		if (!keyboardInputs.contains(code))
		{
			keyboardInputs.add(code);
		}
	}
	
	public void onKeyReleased(KeyEvent e)
	{
		KeyCode code = e.getCode();
		keyboardInputs.remove(code);
	}
	
	public void onMouseClicked(MouseEvent e)
	{
		if (!mouseInputs.contains(e))
		{
			mouseInputs.add(e);			
		}
	}
	
	public void clearMouseEvents()
	{
		mouseInputs.clear();
	}
}